package com.meghan.paymentconfigclient.controller;

import com.meghan.paymentconfigclient.model.Payment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RefreshScope
public class PaymentController {

    @Value("${limit}")
    private String dailyLimit;

    @Value("${db.port}")
    private String dbPort;

    private List<Payment> payments = Stream.of(new Payment(1, "P1", new BigDecimal(1000.00)),
            new Payment(2, "P2", new BigDecimal(5000.00)),
            new Payment(3, "P3", new BigDecimal(3000.00)))
            .collect(Collectors.toList());

    @RequestMapping(value = "/accounts/{accountId}/payments/{paymentId}", method = RequestMethod.GET)
    public @ResponseBody
    List<Payment> getPayment(@PathVariable String accountId, @PathVariable String paymentId) {
        payments.forEach(payment -> payment.setDailyLimit(dailyLimit));
        payments.forEach(payment -> payment.setDbPort(dbPort));
        return payments
                .stream()
                .filter(payment -> (Integer.parseInt(accountId) == payment.getAccountId() && paymentId.equals(payment.getPaymentId())))
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/accounts/{accountId}/payments", method = RequestMethod.GET)
    public @ResponseBody
    List<Payment> getAllPayments(@PathVariable String accountId) {
        payments.forEach(payment -> payment.setDailyLimit(dailyLimit));
        return payments
                .stream()
                .filter(payment -> Integer.parseInt(accountId) == payment.getAccountId())
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/accounts/{accountId}/payments", method = RequestMethod.POST)
    public @ResponseBody
    List<Payment> makePayment(@PathVariable String accountId, @RequestBody Payment payment) {
        payments.forEach(pymt -> pymt.setDailyLimit(dailyLimit));
        payments.add(payment);
        return payments;
    }
}
